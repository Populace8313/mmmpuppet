#!/usr/bin/python3
from gi.repository import GLib
import dbus
import dbus.mainloop.glib



def found_msg(dbus_sms_path, data):
  
  print(f"path {dbus_sms_path} and data{data}")


  return 0


dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
mainloop=GLib.MainLoop()

#Start watching for new messages from Modem Manager.
system_bus = dbus.SystemBus()
system_bus.add_signal_receiver( handler_function = found_msg,
                                bus_name="org.freedesktop.ModemManager1",
                                path = "/org/freedesktop/ModemManager1",
                                dbus_interface = "org.freedesktop.DBus.ObjectManager",
                                signal_name = "InterfacesAdded")
print(f"Watching for \"Added\" Modems.")
system_bus.add_signal_receiver( handler_function = found_msg,
                                bus_name="org.freedesktop.ModemManager1",
                                path = "/org/freedesktop/ModemManager1",
                                dbus_interface = "org.freedesktop.DBus.ObjectManager",
                                signal_name = "InterfacesRemoved")
print(f"Watching for \"Removed\" Modems.")


mainloop.run()
