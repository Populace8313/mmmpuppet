#!/usr/bin/python3


from pathlib import Path
import json
import mmm_log


def sample(sample_config_file):
  """Just a sample Config file. We're not actually going to do
  antyhing with it, just leave it here. I'll have one in the git repo
  also.
  """

  with open(sample_config_file, "w") as open_config_file_w:

    json.dump({
                "homeserver":       "https://matrix-client.matrix.org",
                "bot_account":      "@bot_account:matrix.org",
                "bot_password":     "hunter1",
                "device_name":      "MMMPuppet",
                "matrix_users_csv": "@untidylamp:matrix.org",
                "cell_number":      "+15558881234",
                "cell_country":     "CA",
                "mms_size_limit":   "650000",
    }, open_config_file_w )

def read(config_file):
  """Read the config file and pass a dictionary back of everything in it

  return None - Failure
  return dict - Success
  """
  mmm_log.info("mmm_config.read", "Starting.")


  mmmpuppet_config = None
  
  if Path(config_file).is_file():
    mmm_log.info("mmm_config.read", "found file")
    try:
      with open(config_file, 'r') as open_conf_file_r:
        mmmpuppet_config = json.load(open_conf_file_r)
    except Exception as problem:
      mmm_log.critical("mmm_config.read", problem)

  else:
    mmm_log.info("mmm_config.read", "no file")

  #Test the user has all the things we need in the conf file.
  try:
    mmm_log.debug("mmm_config.read", "homeserver: " + str(mmmpuppet_config['homeserver']) )
    mmm_log.debug("mmm_config.read", "bot_account: " + str(mmmpuppet_config['bot_account']) )
    mmm_log.debug("mmm_config.read", "device_name: " + str(mmmpuppet_config['device_name']) )
    mmm_log.debug("mmm_config.read", "matrix_users_csv: " + str(mmmpuppet_config['matrix_users_csv']) )
    mmmpuppet_config['matrix_users_csv'].split(',')
    mmm_log.debug("mmm_config.read", "cell_number: " + str(mmmpuppet_config['cell_number']) )
    mmm_log.debug("mmm_config.read", "cell_country: " + str(mmmpuppet_config['cell_country']) )
    mmm_log.debug("mmm_config.read", "mms_size_limit: " + str(mmmpuppet_config['mms_size_limit']) )


    if( "access_token" in mmmpuppet_config ):
      mmm_log.debug("mmm_config.read", "access_token: " + str(mmmpuppet_config['access_token']) )
      mmm_log.debug("mmm_config.read", "device_id: " + str(mmmpuppet_config['device_id']) )


      #Don't allow bot_password AND access_token in the config file.
      if( "bot_password" in mmmpuppet_config ):
        mmm_log.critical("mmm_config.read", "We have a token in the file, remove the bot_password." )
        return "check the config" 

    else:
      mmm_log.debug("mmm_config.read", "bot_password: " + str(mmmpuppet_config['bot_password']) )


  except Exception as problem:
    mmm_log.critical("mmm_config.read", "Check your conf file. " + str(problem))
    return "check the config"


  return mmmpuppet_config

def write(config_file, config_dict):
  """Write the Config file to disk. This will be to remove the
  password and add the token.

  Return None - Failure
  return 0    - Success.
  """


  if Path(config_file).is_file():

    #Open config file to write to.
    try: 
      with open(config_file, "w") as open_config_file_w:

        # write the login details to disk
        json.dump(config_dict, open_config_file_w )

      return 0

    except Exception as problem:
      mmm_log.critical("mmm_config.read", problem)
      return None

  else:
    mmm_log.error("mmm_config.write", "Cant Find config file.")
    return None
